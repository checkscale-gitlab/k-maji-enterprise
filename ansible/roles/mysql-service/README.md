mysql-service
=============

Tasks for setting up mysql server.

Requirements
------------

CentOS Linux 7 (Core) based system.

Role Variables
--------------

# These are for mysql server
mysql_root_user: root
mysql_root_password: s3cr3t0#@LP

# You shouldn't need to change this.
mysql_port: 3306

Dependencies
------------

Not at the moment.

Example Playbook
----------------

To use this role, just include it in your playbook, for example:

    - hosts: servers
      roles:
         - mysql-service

License
-------

MIT

Author Information
------------------

Please any question, please contact the autor at: jorge.medina@kronops.com.mx.
