yum-setup
=========

Tasks for setting up YUM client.

Requirements
------------

EL based system.

Role Variables
--------------

rhsm_user: foo@bar
rhsm_pass: s3cr3t

Dependencies
 -----------

Not at the moment.

Example Playbook
----------------

To use this role, just include it in your playbook, for example:

    - hosts: servers
      roles:
         - yum-setup

License
-------

MIT

Author Information
------------------

Please any question, please contact the autor at: jorge.medina@kronops.com.mx.
