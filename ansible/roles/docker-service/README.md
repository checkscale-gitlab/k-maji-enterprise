docker-service
==============

Deploy docker and docker compose.

Requirements
------------

EL based system.

Role Variables
--------------

Dependencies
------------

Example Playbook
----------------

To use this role, just include it in your playbook, for example:

    - hosts: servers
      roles:
         - docker-service

License
-------

MIT

Author Information
------------------

Please any question, please contact the autor at: jorge.medina@kronops.com.mx.
