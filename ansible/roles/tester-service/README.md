jenkins-service
===============

Tasks for setting up a Python Tester service.

Requirements
------------

EL based system.

Role Variables
--------------

tester_role_path: "roles/tester-service"

pip modules:
  - selenium
  - termcolor
  - terminaltables
  - xmlrunner
  - testinfra
  - ansible
  - locust

Dependencies
------------

Not at the moment.

Example Playbook
----------------

To use this role, just include it in your playbook, for example:

    - hosts: servers
      roles:
        - tester-service

License
-------

MIT

Author Information
------------------

Please any question, please contact the autor at: jorge.medina@kronops.com.mx.
