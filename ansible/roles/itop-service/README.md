itop-service
============

Tasks for setting up itop service.

Requirements
------------

EL based system.

Role Variables
--------------

# These are the iTop database settings
mysql_root_user: root
itop_mysql_db: itopdb
itop_mysql_user: itopdbuser
itop_mysql_password: s3cr3t0#@LP

# Which version of iTop to deploy
itop_version: 2.0.6
itop_release: 2.0.6-4294
itop_url_download: http://downloads.sourceforge.net/project/itop/itop/2.6.0/iTop-2.6.0-4294.zip
itop_sha256sum: 07935a17bfb190e6680b30431d1527fe986266aff7ec64efb880f12f460d67a9
itop_datamodel_toolkit_version: 2.3
itop_teamip_module_version: 2.0.3
itop_teamip: teemip-core-ip-mgmt-2.3.0.zip

# Webdev user access
itop_webdev_group: webdevs
itop_webdev_username: webdevmaster
itop_webdev_userpass_hash: $6$VcxKQL1CXwSfmWk$f8Ye1TGIesw85UUUo/POz4caN1Szx7QWkCzsXJ.BfT32MdmsMhxDM7R/A3G.n81TKaiLi//.AoOUpYMAkpJ5D/
itop_root: /var/www/{{ dns_public_domain }}/htdocs

# Apache httpd conf
apache_root: /var/www/{{ dns_public_domain }}
apache_conf_file: /etc/httpd/conf/httpd.conf
apache_user: apache
apache_group: apache
apache_service_name: httpd
apache_sites_available_dir: /etc/httpd/sites-available
apache_sites_enabled_dir: /etc/httpd/sites-enabled
php_init_dir: /etc/php.d


Dependencies
------------

This role depends on the follow roles:

 * httpd-service
 * mysql-service

Example Playbook
----------------

To use this role, just include it in your playbook, for example:

    - hosts: servers
      roles:
        - itop-service

License
-------

MIT

Author Information
------------------

Please any question, please contact the autor at: jorge.medina@kronops.com.mx.
