<p align="center">
  <img src="https://gitlab.com/kronops/images/raw/master/images/kamaji.jpg" width=150>
</p>

# K-MAJI Enterprise
> DevOps Orchestration

K-Maji Enterprise es una plataforma de orquestación de procesos de TI, está
diseñada para ayudar a los administradores de sistemas, desarrolladores de
software y otras áreas de ingeniería a simplificar los procesos de desarrollo
y operaciones de productos digitales.

## Instalación

Para detalles de la implementación de K-MAJI, favor de referirse a la
[documentación](https://kronops.gitlab.io/enterprise-docs).

## Construido con

* [CentOS 7](https://www.centos.org/) - GNU-Linux Operating System
* [Bash Script](https://www.gnu.org/software/bash/) - GNU-Linux command scripting
* [Python3](https://www.python.org) - Programming Languaje
* [Ansible](https://www.ansible.com) - TI Automation
* [Packer](https://www.packer.io) - Machine Images Creation
* [Jenkins](https://www.jenkins.io) - CI Automation
* [Chef InSpec](https://www.inspec.io) - Audit and Automated Testing Framework
* [Selenium](https://www.seleniumhq.org) - Web Browser Automation

## Contribuyendo

Por favor lee el
[CONTRIBUTING.md](https://gitlab.com/kronops/k-maji-enterprise/blob/master/CONTRIBUTING.md)
si deseas contribuir. Conoce nuestro código de
[conducta](https://gitlab.com/kronops/k-maji-enterprise/blob/master/CODE_OF_CONDUCT.md).
Usamos el
[gitlab flow](https://docs.google.com/presentation/d/1M4oRUFyz4Wlc1TN7MpkHyyKQPlW2fnZrXwyKiJIB_cg/edit?usp=sharing)
para integrar un merge request al proyecto.

## Versionado

K-MAJI Enterprise tiene versiones según [SemVer](https://semver.org).

## Autores

* **Axel Herrera** - *Trabajo Inicial* - https://gitlab.com/axel.herrera
* **Jorge Medina** - *Trabajo Inicial* - https://gitlab.com/jorge.medina
* **Luis Carrillo** - *Documentación* - https://gitlab.com/Narratore
* **Esau Garcia** - *Documentación* - https://gitlab.com/35AU

También puede consultar la lista de todos los
[contribuyentes](https://gitlab.com/kronops/k-maji-enterprise/-/graphs/master)
quienes han participado en este proyecto.

## Licencia

Este proyecto está bajo la Licencia MIT - mire el archivo
[LICENSE.md](https://gitlab.com/kronops/k-maji-enterprise/blob/master/LICENSE.md)
para detalles.
