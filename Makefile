#
# Makefile for K-MAJI Enterprise
#
# author: jorge.medina@kronops.com.mx
# desc: Script to build the vagrant development environment.

include .env

.PHONY: all vagrant bootstrap build test deploy clean destroy help

all: help

vagrant:
	@echo "Building ${PROJECT} ${PROJECT_ENV} environment based on vagrant."
	vagrant up
	vagrant status
	vagrant ssh

build:
	bin/build.sh ${PROJECT} ${PROJECT_ENV}

bootstrap:
	bin/bootstrap.sh

test:
	ansible-playbook ansible/playbooks/*.yml --syntax-check -e "TARGET=None"

deploy:
	ansible-playbook ansible/playbooks/deploy-general-system.yml -e "TARGET=central_deployer"
	ansible-playbook ansible/playbooks/deploy-deployer.yml
	ansible-playbook ansible/playbooks/deploy-general-system.yml -e "TARGET=central_tester"
	ansible-playbook ansible/playbooks/deploy-tester.yml --skip-tags "app_deploy"

clean:
	@echo "Cleaning ${PROJECT} ${PROJECT_ENV} environment."
	rm -rf *.retry *.log

destroy:
	@echo ""
	@echo "Destroying ${PROJECT} ${PROJECT_ENV} environment."
	@echo ""
	vagrant destroy -f

help:
	@echo ""
	@echo "Please use 'make <target>' where <target> is one of:"
	@echo ""
	@echo "  vagrant   Provision the ${PROJECT} ${PROJECT_ENV} environment."
	@echo "  bootstrap Bootstrap the ${PROJECT} ${PROJECT_ENV} environment."
	@echo "  build     Builds the ${PROJECT} ${PROJECT_ENV} environment."
	@echo "  test      Tests for the ${PROJECT} ${PROJECT_ENV} environment."
	@echo "  deploy    Deploy the ${PROJECT} ${PROJECT_ENV} environment."
	@echo "  clean     Cleans ${PROJECT} changes on ansible settings."
	@echo "  destroy   Destroys the ${PROJECT} ${PROJECT_ENV} environment."
	@echo ""
	@echo ""
