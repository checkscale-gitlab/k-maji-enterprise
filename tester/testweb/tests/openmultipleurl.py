# -*- coding: UTF-8 -*-

import os
import requests
from time import sleep as t

from unittest import TestCase

from functions import functions as f

class Openmultipleurl(TestCase):
    def test_app(s):
        protocol = 'http://%s'
        for num,uri in enumerate(os.getenv('urls').split(',')):
            url = protocol % uri
            s.wd.get(url)
            screen = f.getScreenshot(s.wd, '%i.png' % num)
            req = requests.get(url)
            loadStatus = True if req.status_code == 200 else False
            s.output.append([
              f.blue(url),
              f.green(req.status_code),
              f.green('OK') if loadStatus else f.red('FAIL')
              ])
        if loadStatus is False: f.quitExecution(s.wd)
