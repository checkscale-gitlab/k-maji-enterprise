
import os

build_number = os.getenv('BUILD_NUMBER', '0')
jenkins_workspace = os.getenv('WORKSPACE')
screenshotPathToSave = jenkins_workspace if jenkins_workspace else '%s/imagenes/' % os.getcwd()
xmlPathToSave = jenkins_workspace if jenkins_workspace else '%s/xmlresult/' % os.getcwd()
chromeDriverPath = '/usr/local/bin/chromedriver'
